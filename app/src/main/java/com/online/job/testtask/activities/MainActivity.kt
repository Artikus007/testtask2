package com.online.job.testtask.activities

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.online.job.testtask.R
import com.online.job.testtask.data.UserPreferences
import com.online.job.testtask.dialogs.SelectPairsDialogFragment
import com.online.job.testtask.loaders.ItemLoader
import kotlinx.android.synthetic.main.activity_tasks.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tasks)

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.navigationIcon?.setColorFilter(
                ContextCompat.getColor(this, R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP)
        }

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""


        date_from_text.setOnClickListener {
            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    val date = String.format( "%d-%d-%d" ,dayOfMonth,monthOfYear+1,year)
                    date_from_text.setText(date)
                    val formatter = SimpleDateFormat("dd-MM-yyyy",Locale.getDefault())
                    val mDate = formatter.parse(date) as Date
                    ItemLoader.from = mDate.time/1000
                    ItemLoader.getItems()
                },
                mYear,
                mMonth,
                mDay
            )
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }
        date_to_text.setOnClickListener {
            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    val date = String.format( "%d-%d-%d" ,dayOfMonth,monthOfYear+1,year)
                    date_to_text.setText(date)
                    val formatter = SimpleDateFormat("dd-MM-yyyy",Locale.getDefault())
                    val mDate = formatter.parse(date) as Date
                    ItemLoader.to = mDate.time/1000
                    ItemLoader.getItems()
                },
                mYear,
                mMonth,
                mDay
            )
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

        choose_pairs.setOnClickListener {
            SelectPairsDialogFragment().show(supportFragmentManager)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_logout->{
                ItemLoader.token = ""
                UserPreferences.getInstance(applicationContext).userToken = ""
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }
}