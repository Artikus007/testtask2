package com.online.job.testtask.data

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.StringRes
import androidx.preference.PreferenceManager
import com.online.job.testtask.R
import com.online.job.testtask.utils.SingletonHolder

class  UserPreferences(private val mContext: Context) {
    var mPrefs : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)


    @Synchronized
    private fun getBoolean(@StringRes keyRes: Int, defaultValue: Boolean): Boolean {
        return mPrefs.getBoolean(mContext.getString(keyRes), defaultValue)
    }

    @Synchronized
    private fun getInt(@StringRes keyRes: Int, defaultValue: Int): Int {
        var i = 0
        try {
            i = mPrefs.getInt(mContext.getString(keyRes), defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return i
    }

    @Synchronized
    private fun getLong(@StringRes keyRes: Int, defaultValue: Long): Long {
        var l: Long = 0
        try {
            l = mPrefs.getLong(mContext.getString(keyRes), defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return l
    }

    @Synchronized
    private fun getString(@StringRes keyRes: Int, defaultValue: String): String {
        var s = ""
        try {
            s = mPrefs.getString(mContext.getString(keyRes), defaultValue)?:""
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return s
    }


    @Synchronized
    private fun putBoolean(@StringRes keyRes: Int, value: Boolean) {
        try {
            mPrefs.edit()
                .putBoolean(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Synchronized
    private fun putInt(@StringRes keyRes: Int, value: Int) {
        try {
            mPrefs.edit()
                .putInt(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Synchronized
    private fun putLong(@StringRes keyRes: Int, value: Long) {
        try {
            mPrefs.edit()
                .putLong(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Synchronized
    private fun putString(@StringRes keyRes: Int, value: String) {
        try {
            mPrefs.edit()
                .putString(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    var userLogin: String
        get() = getString(R.string.pref_key_login, "")
        set(more) = putString(R.string.pref_key_login, more)

    var userPass: String
        get() = getString(R.string.pref_key_pass, "")
        set(more) = putString(R.string.pref_key_pass, more)

    var userToken: String
        get() = getString(R.string.pref_key_token, "")
        set(more) = putString(R.string.pref_key_token, more)

    companion object : SingletonHolder<UserPreferences, Context>( {
        UserPreferences(it.applicationContext)
    })
}
