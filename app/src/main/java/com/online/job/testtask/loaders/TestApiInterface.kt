package com.online.job.testtask.loaders

import com.online.job.testtask.Constants
import com.online.job.testtask.helpers.LogHelper
import com.online.job.testtask.model.Item
import com.online.job.testtask.model.User
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface TestApiInterface {

    @Headers("Content-Type: application/json", "Accept: application/json" )
    @POST("/api/Authentication/RequestMoblieCabinetApiToken")
    fun login(@Body user: User) : Observable<String>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("/clientmobile/GetAnalyticSignals/{userId}")
    fun getInfo(@Path("userId") userId : String,
                @Query("tradingsystem") systemNumber : Int,
                @Query("pairs") pairs :  String ,
                @Query("from") from : Long,
                @Query("to") to : Long
    ) : Observable<ArrayList<Item>>


    companion object  {
        fun create(client : OkHttpClient = OkHttpClient()):  Observable<TestApiInterface> {

            val retrofit = retrofit2.Retrofit.Builder()
                .client( client)
                .baseUrl(Constants.TEST_API)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return Observable.just(retrofit.create(TestApiInterface::class.java))

        }
        fun createWithHeader(header : String, value : String) :  Observable<TestApiInterface> {
           return create(OkHttpClient
                .Builder()
                .addInterceptor{
                    LogHelper.d("ItemLoader",  "url " + it.request().url())
                    var request = it.request()

                    val headers = request.headers().newBuilder().add(header, value).build()
                    request = request.newBuilder().headers(headers).build()
                    val response = it.proceed(request)
                    val code = response.code()

                    LogHelper.d("ItemLoader",  "response code " + code)
                    if(code != 200 && code != 201){
                        ItemLoader.token = ""
                        ItemLoader.error.onNext("Error: " + response.message())
                    }
                    response
                }
                .build())
        }

    }
}