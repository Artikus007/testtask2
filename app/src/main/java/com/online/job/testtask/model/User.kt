package com.online.job.testtask.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User (
    @Expose
    @SerializedName("Login")
    var login : String,
    @Expose
    @SerializedName("Password")
    var password : String
)