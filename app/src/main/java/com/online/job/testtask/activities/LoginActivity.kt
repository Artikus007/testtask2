package com.online.job.testtask.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.online.job.testtask.R
import com.online.job.testtask.data.UserPreferences
import com.online.job.testtask.loaders.ItemLoader

class LoginActivity : AppCompatActivity(){



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val pref = UserPreferences.getInstance(applicationContext)
        if(pref.userToken.isNotEmpty()  ){
            ItemLoader.token = pref.userToken
            ItemLoader.userId = pref.userLogin
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            return
        }
        setContentView(R.layout.activity_login)
       // val user = User("20234561","ladevi31")
    }


}