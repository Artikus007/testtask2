package com.online.job.testtask.loaders

import com.online.job.testtask.helpers.LogHelper
import com.online.job.testtask.model.Item
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject


object ItemLoader {

    val items = BehaviorSubject.create<ArrayList<Item>>()
    val error = BehaviorSubject.create<String>()

    val loading = BehaviorSubject.create<Boolean>()

 //   val info = "/clientmobile/GetAnalyticSignals/20234561?tradingsystem=3&pairs=EURUSD,GBPUSD,USDJPY,USDCHF,USDCAD,AUDUSD,NZDUSD&from=1479860023&to=1480066860"
 //   val auth = "/api/Authentication/RequestMoblieCabinetApiToken"

    var token = ""
    var pairs = "EURUSD,GBPUSD,USDJPY,USDCHF,USDCAD,AUDUSD,NZDUSD"
    var from = 1479860023L
    var to = 1480066860L
    var userId = "20234561"

    @Synchronized
    fun getItems()  {
        if (token.isEmpty()){
            error.onNext("Error: token is empty" )
            return
        }
        if (userId.isEmpty()){
            error.onNext("Error: userId is empty" )
            return
        }
        LogHelper.d(ItemLoader,"token $token")
        LogHelper.d(ItemLoader,"userId $userId")
        loading.onNext(true)
        CompositeDisposable().add(
            TestApiInterface.createWithHeader("passkey",token)
            .observeOn(Schedulers.io())
            .flatMap{  withHeader->
                withHeader.getInfo(userId,3,
                    pairs,
                    from,
                    to
                )
            }
            .subscribe({
                items.onNext(it)
                loading.onNext(false)
                LogHelper.d(ItemLoader,"data $it")
            },{e->
                e.printStackTrace()
                loading.onNext(false)
            })
        )
    }

}