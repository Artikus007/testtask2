package com.online.job.testtask.fragments

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import com.google.android.material.textfield.TextInputLayout
import com.online.job.testtask.R
import com.online.job.testtask.activities.LoginActivity
import com.online.job.testtask.activities.MainActivity
import com.online.job.testtask.data.UserPreferences
import com.online.job.testtask.helpers.LogHelper
import com.online.job.testtask.helpers.UserHelper
import com.online.job.testtask.loaders.ItemLoader
import com.online.job.testtask.loaders.TestApiInterface
import com.online.job.testtask.model.User
import com.online.job.testtask.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.view.*


class AuthFragment : BaseFragment()  {

    private lateinit var login : EditText
    private lateinit var password : EditText
    private lateinit var emailForm : TextInputLayout
    private lateinit var passwordForm : TextInputLayout
    private lateinit var progressBar : ProgressBar
    private lateinit var loginBtn : Button

    private var user = User("","")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val v = LayoutInflater.from(context).inflate(R.layout.fragment_login, container, false)
        login = v.login
        emailForm = v.email_login_form
        password = v.password
        passwordForm = v.email_login_form
        progressBar = v.login_progress
        loginBtn = v.email_sign_in_button
        loginBtn.setOnClickListener{attemptLogin()}

        context?.let {
            UserPreferences.getInstance(it).let {prefs->
                login.setText(prefs.userLogin)
                password.setText(prefs.userPass)
            }
        }

        return v
    }


    private fun showError(err : String) {
        toast(context, err)
        showProgress(false)
    }


    private fun attemptLogin() {

        emailForm.error = null
        passwordForm.error = null

        user.login = login.text.toString()
        user.password = password.text.toString()

        if (!UserHelper.isPasswordValid(user.password)  ) {
            passwordForm.error = getString(R.string.error_invalid_password)
        }

        if(emailForm.error == null && passwordForm.error == null){
            showProgress(true)

            disposable.add(
                TestApiInterface.create()
                    .observeOn(Schedulers.io())
                    .flatMap {
                        it.login(user)
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({token->
                        LogHelper.d(ItemLoader,"token $token")
                        context?.let {
                         if(token.isNotEmpty()  ) {
                                ItemLoader.token = token
                                ItemLoader.userId = user.login
                                UserPreferences.getInstance(it).userToken = token
                                UserPreferences.getInstance(it).userLogin = user.login
                                UserPreferences.getInstance(it).userPass = user.password
                                showProgress(false)
                                 val intent = Intent(activity, MainActivity::class.java)
                                 startActivity(intent)
                         }else{
                                ItemLoader.token = ""
                                UserPreferences.getInstance(it).userToken = ""
                                showError("Error")
                            }
                        }
                    },{e->
                        context?.let {
                            ItemLoader.token = ""
                            UserPreferences.getInstance(it).userToken = ""
                        }

                        LogHelper.d(ItemLoader,"Error: " + e.message)
                        showError("Error: " + e.message)
                    })
            )
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
          progressBar.visibility = if (show) View.VISIBLE else View.INVISIBLE
    }
}