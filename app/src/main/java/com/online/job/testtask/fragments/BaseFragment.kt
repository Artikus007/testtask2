package com.online.job.testtask.fragments


import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment : Fragment() {

 val disposable  = CompositeDisposable()

 override fun onDestroyView() {
     super.onDestroyView()
     disposable.clear()
 }

}