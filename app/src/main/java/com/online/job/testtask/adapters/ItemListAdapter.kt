package com.online.job.testtask.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.online.job.testtask.R
import com.online.job.testtask.model.Item
import kotlinx.android.synthetic.main.instance_task.view.*
import java.util.*

class ItemListAdapter(var tasks : Vector<Item> = Vector()) : RecyclerView.Adapter<ItemHolder>(){


    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int): ItemHolder {
        return ItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.instance_task, parent, false))
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun onBindViewHolder(holder : ItemHolder, position: Int) {
        if(tasks.size > 0 && position < tasks.size){
            holder.onBind(tasks[position])
        }
    }

}

class ItemHolder(private val view : View): RecyclerView.ViewHolder(view){

    fun onBind(item : Item){
        view.valActualTime.text = item.ActualTime.toString()
        view.valCmd.text = item.Cmd.toString()
        view.valComment.text = item.Comment
        view.valPair.text = item.Pair
        view.valPeriod.text = item.Period
        view.valPrice.text = item.Price.toString()
        view.valPrice.text = item.Price.toString()
        view.valSl.text = item.Sl.toString()
        view.valTp.text = item.Tp.toString()
        view.valTradingSystem.text = item.TradingSystem.toString()
    }

}

