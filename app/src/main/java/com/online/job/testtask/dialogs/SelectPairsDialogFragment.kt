package com.online.job.testtask.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.online.job.testtask.R
import com.online.job.testtask.loaders.ItemLoader
import java.lang.StringBuilder


class SelectPairsDialogFragment : BaseDialogFragment()  {


    private val mItemsSelect = arrayOf( "EURUSD","GBPUSD","USDJPY","USDCHF","USDCAD","AUDUSD","NZDUSD")
    private val mItemsVal = booleanArrayOf( true,true,true,true,true,true,true)


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return context?.let {

          AlertDialog.Builder(it)
                    .setTitle(R.string.select_pairs)
                    .setMultiChoiceItems(mItemsSelect, mItemsVal)
                    { _ : DialogInterface, pos :Int, isChecked : Boolean->
                        mItemsVal[pos] = isChecked
                    }
                    .setPositiveButton(R.string.action_select)  { _ : DialogInterface, _ :Int->
                         val str = StringBuilder()
                         for ((i, isChecked) in mItemsVal.withIndex()){
                             if(isChecked){
                                 str.append(mItemsSelect[i])
                                 str.append(',')
                             }
                         }
                         str.removeSuffix(",")
                        ItemLoader.pairs = str.toString()
                        ItemLoader.getItems()
                    }
                    .setNegativeButton(R.string.action_cancel, null)
                    .show()


        }?:super.onCreateDialog(savedInstanceState)
    }


}
