package com.online.job.testtask.dialogs

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

abstract class BaseDialogFragment : DialogFragment() {


    fun show(manager: FragmentManager) {
        show(manager,this::class.java.name)
    }
    override fun show(manager: FragmentManager, tag: String) {
        if (manager.findFragmentByTag(this::class.java.name) == null)
        try {
            val ft = manager.beginTransaction()
            ft.add(this, tag)
            ft.commitAllowingStateLoss()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {

    }
}
