package com.online.job.testtask.model

import com.google.gson.annotations.Expose

data class Item (
    @Expose
    val ActualTime: Int,
    @Expose
    val Cmd: Int,
    @Expose
    val Comment: String,
    @Expose
    val Id: Int,
    @Expose
    val Pair: String,
    @Expose
    val Period: String,
    @Expose
    val Price: Double,
    @Expose
    val Sl: Double,
    @Expose
    val Tp: Double,
    @Expose
    val TradingSystem: Int
)