package com.online.job.testtask.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.online.job.testtask.R
import com.online.job.testtask.activities.LoginActivity
import com.online.job.testtask.activities.MainActivity
import com.online.job.testtask.adapters.ItemListAdapter
import com.online.job.testtask.data.UserPreferences
import com.online.job.testtask.helpers.LogHelper
import com.online.job.testtask.loaders.ItemLoader
import com.online.job.testtask.model.Item
import com.online.job.testtask.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.list.view.*

class ListFragment : BaseFragment() {

      lateinit var recyclerView: RecyclerView

      lateinit var refreshLayout : SwipeRefreshLayout

      private val mAdapter = ItemListAdapter()

        override fun onStart() {
            super.onStart()
            ItemLoader.getItems()
        }

      override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

          val view = LayoutInflater.from(context).inflate(R.layout.fragment_tasks, container, false)

          recyclerView =  view.list
          refreshLayout = view.refreshLayout

          refreshLayout.setColorSchemeColors(  resources.getColor(R.color.colorAccent) )

          refreshLayout.setOnRefreshListener{
              LogHelper.d(this,"Refresh")
              ItemLoader.getItems()
          }
          recyclerView.layoutManager =  LinearLayoutManager(context)

          recyclerView.adapter = mAdapter

          disposable.addAll(ItemLoader.items
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe({items->
                   notifyAdapter(items)
              },{e->e.printStackTrace()}),
              ItemLoader.error
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe({err->
                      toast(context,err)
                      context?.let {
                          UserPreferences.getInstance(it).let { prefs->
                              prefs.userToken = ""
                              val intent = Intent(activity, LoginActivity::class.java)
                              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                              startActivity(intent)
                          }
                      }
                  },{e->
                      e.printStackTrace()

                  }),
            ItemLoader.loading
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({loading->
                    refreshLayout.isRefreshing = loading
                },{e->e.printStackTrace()})
          )

          return view
      }


    @Synchronized
    private fun notifyAdapter(items : ArrayList<Item>){
        mAdapter.tasks.clear()
        mAdapter.tasks.addAll(items)
        mAdapter.notifyDataSetChanged()
    }


  }